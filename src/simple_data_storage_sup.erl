-module(simple_data_storage_sup).

-behaviour(supervisor).

%% API
-export([start_link/0, start_link/1]).

%% Supervisor callbacks
-export([init/1]).

%% Helper macro for declaring children of supervisor
-define(CHILD_WITH_ARGS(I, Type, Args), {I, {I, start_link, Args}, permanent, 5000, Type, [I]}).

%% ===================================================================
%% API functions
%% ===================================================================

start_link(Args) ->
  supervisor:start_link({local, ?MODULE}, ?MODULE, Args).

start_link() ->
  supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init([]) ->
  SDSServer = ?CHILD_WITH_ARGS(simple_data_storage_server, worker, []),
  {ok, { {one_for_one, 5, 10}, [SDSServer]} };
init(Args) ->
  SDSServer = ?CHILD_WITH_ARGS(simple_data_storage_server, worker, Args),
  {ok, { {one_for_one, 5, 10}, [SDSServer]} }.
