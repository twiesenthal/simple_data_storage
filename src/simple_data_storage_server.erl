-module(simple_data_storage_server).

-behaviour(gen_server).

-export([start_link/0, start_link/1, store/1, store/2, get/1, get_filtered/1, delete/1, clear/0, keys/0, configure/1]).

-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3]).

-define(DEFAULT_TABLE_NAME, sds).
-define(DEFAULT_DETS_FILE, "./sds.dets").
-define(DETS_AUTOSAVE, 30000).
-define(GEN_CALL_TIMEOUT, 3000).

start_link(C) when is_list(C)->
  gen_server:start_link({local, ?MODULE}, ?MODULE, C, []).

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

init([]) ->
  create_tables(?DEFAULT_TABLE_NAME, ?DEFAULT_DETS_FILE);
init(L) when is_list(L) ->
  FileName = proplists:get_value(file_name, L, ?DEFAULT_DETS_FILE),
  TableName = proplists:get_value(table_name, L, ?DEFAULT_TABLE_NAME),
  create_tables(TableName, FileName).

store(K,V) ->
  gen_server:call(?MODULE, {store, [{K,V}]}, ?GEN_CALL_TIMEOUT).

store(L) when is_list(L)->
  gen_server:call(?MODULE, {store, L}, ?GEN_CALL_TIMEOUT).

get(K) ->
  gen_server:call(?MODULE, {get,K}, ?GEN_CALL_TIMEOUT).

get_filtered(Filter) ->
  gen_server:call(?MODULE, {get_filterd,Filter}, ?GEN_CALL_TIMEOUT).

delete(K) ->
  gen_server:call(?MODULE, {delete, K}, ?GEN_CALL_TIMEOUT).

clear() ->
  gen_server:call(?MODULE, clear, ?GEN_CALL_TIMEOUT).

keys() ->
  gen_server:call(?MODULE, keys, ?GEN_CALL_TIMEOUT).

configure(C) when is_list(C) ->
  FileName = proplists:get_value(file_name, C, ?DEFAULT_DETS_FILE), 
  TableName = proplists:get_value(table_name, C, ?DEFAULT_TABLE_NAME),
  Data = [{table_name, TableName}, {file_name, FileName}], 
  gen_server:call(?MODULE, {configure, Data}, ?GEN_CALL_TIMEOUT).

%% callbacks
handle_call({store, L}, _From, State) when is_list(L) ->
  Reply = store_data(L, State),
  {reply, Reply, State};
handle_call({get, K}, _From, State) ->
  Tid = proplists:get_value(ets_tid, State),
  case ets:lookup(Tid, K) of
    [{K,V}] ->
      {reply, V, State};
    _ ->
      {reply, undefined, State}
  end;
handle_call({get_filterd, F}, _From, State) ->
  Tid = proplists:get_value(ets_tid, State),
  case ets:match(Tid, F) of
    [] ->
      {reply, [], State};
    H ->
      {reply, H, State}
  end;
handle_call(keys, _From, State) ->
  Tid = proplists:get_value(ets_tid, State),
  V = ets:match(Tid, {'$1', '_'}),
  {reply, V, State};
handle_call({configure, Data}, _From, State) ->
  FileName = proplists:get_value(file_name, Data, ?DEFAULT_DETS_FILE),
  TableName = proplists:get_value(table_name, Data, ?DEFAULT_TABLE_NAME),
  close(State),
  {ok, NewState} = create_tables(TableName, FileName),
  {reply, ok, NewState};
handle_call({delete, K}, _From, State) ->
  Tid = proplists:get_value(ets_tid, State),
  Dets_table = proplists:get_value(dets_table_name, State),
  ets:delete(Tid, K),
  dets:delete(Dets_table, K),
  {reply, ok, State};
handle_call(clear, _From, State) ->
  Dets_table = proplists:get_value(dets_table_name, State),
  Tid = proplists:get_value(ets_tid, State),
  dets:delete_all_objects(Dets_table),
  ets:delete_all_objects(Tid),
  {reply, ok, State};
handle_call(_Request, _From, State) ->
  Reply = {error, "no such callback"},
  {reply, Reply, State}.

handle_cast(_Msg, State) ->
  {noreply, State}.

handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, State) ->
 close(State),
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%private

store_data([], _State) ->
  ok;
store_data([H|T], State) ->
  {K,V} = H,
  Tid = proplists:get_value(ets_tid, State),
  Dets_table = proplists:get_value(dets_table_name, State),
  ets:insert(Tid, {K,V}),
  dets:insert(Dets_table, {K,V}),
  store_data(T, State) .


create_tables(TableName, FileName) ->
  Args = [
    {file, FileName},
    {auto_save, ?DETS_AUTOSAVE}
  ],
  case dets:open_file(TableName, Args) of
    {ok, TableName} ->
      Ets = ets:new(TableName,[private, named_table]),
      ets:from_dets(Ets, TableName),
      {ok, [{dets_table_name, TableName}, 
            {ets_tid, Ets},
            {file_name, FileName}]};
    {error, Reason} ->
      error_logger:error_msg("~w~n",[Reason]),
      {error, Reason}
  end.

close(State) ->
  DetsTable 
= proplists:get_value(dets_table_name, State),
  Tid = proplists:get_value(ets_tid, State),
  ets:delete(Tid),
  dets:close(DetsTable).